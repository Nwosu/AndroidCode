package com.example.enwosu.asyntaskclouddataparser;

/**
 * Created by enwosu on 12/28/2014.
 * This class will store the data that we get from the network source
 */
public class DataObject {
    private String Latitude;
    private String Longitude;
    private String RoadName;
    private String Title;
    private String ImageURL;

    public DataObject(String latitude, String longitude, String roadName, String title, String imageURL) {
        Latitude = latitude;
        Longitude = longitude;
        RoadName = roadName;
        Title = title;
        ImageURL =  imageURL;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        this.Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        this.Longitude = longitude;
    }

    public String getRoadName() {
        return RoadName;
    }

    public void setRoadName(String roadName) {
        this.RoadName = roadName;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        this.ImageURL = imageURL;
    }

    // This is what will show up in each row of the list view
    public String toString(){
        return "Latitude: " + this.Latitude + "\n" +
                "Longitude: " + this.Longitude + "\n" +
                "RoadName: " + this.RoadName + "\n" +
                "ImageURL: " + this.ImageURL + "\n" +
                "Title: " + this.Title + "\n";
    }
}
