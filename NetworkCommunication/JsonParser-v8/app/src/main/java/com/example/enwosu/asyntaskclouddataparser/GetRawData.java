package com.example.enwosu.asyntaskclouddataparser;

import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by enwosu on 4/3/2016.
 * This class will download data in either XML or JSON format
 */

// Has all the possible states that this class can be in
enum DownloadStatus{IDLE, PROCESSING, NOT_INITIALIZED, FAILED_OR_EMPTY, OK}

public class GetRawData {
    private String LOG_TAG = GetRawData.class.getSimpleName();
    // mRawURL will be used to store the URL from where we plan to download the data.
    private String mRawURL;
    // mData will be used to store the downloaded data.
    private String mData;
    // mDownloadStatus will be used to store the state of the download process.
    private DownloadStatus mDownloadStatus;

    /**
     * WHAT'S GOING ON IN THIS CONSTRUCTOR METHOD:
     * This constructor is used to initialize this class by:
     * (1) Reset the relevant variables.
     * (2) Getting the URL which points to the eternal data location
     * (3) The constructor is also used to set an initial status of the Download process
     */
    public GetRawData(String mRawURL){
        /**
         * (1) Reset the relevant variables.
         * Note: This only resets the variables for this class and not the URL variable passed to the GetRawData i.e
         * - The following variables are reset
         * -- this.mRawURL and NOT the mRawURL parameter passed to this constructor i.e. NOT "mRawURL" in "GetRawData(mRawURL)"
         * -- this.mData
         * -- this.mDownloadStatus
         */
        reset();
        /**
         * (2) Getting the URL which points to the eternal data location. Here, we set "this.URL" which is local to this class to the
         * "mRawURL" parameter passed tot he constructor.
         */
        this.mRawURL = mRawURL;
        /**
         * (3) The constructor is also used to set an initial status of the Download process
         */
        this.mDownloadStatus = DownloadStatus.IDLE;
    }
    /**
     * WHAT'S GOING ON IN THIS reset() METHOD:
     * Reset the following variables, all of which are local to this class
     * (1) this.mDownloadStatus
     * (2) this.mRawURL and NOT the mRawURL parameter passed to this constructor i.e. NOT "mRawURL" in "GetRawData(mRawURL)"
     * (3) this.mData
     */
    public void reset(){
        /**
         * (1) Reset this.mDownloadStatus
         */
        this.mDownloadStatus = DownloadStatus.IDLE;
        /**
         * (2) Reset this.mRawURL
         */
        this.mRawURL = null;
        /**
         * (3) Reset this.mData
         */
        this.mData = null;
    }

    // This getter will be used to access the downloaded data (stored in this.mData) from within or outside this class.
    public String getmData(){
        return mData;
    }

    // This getter will be used to get the status of the Download process (stored in this.mDownloadStatus) from within or outside this class.
    public DownloadStatus getmDownloadStatus(){
        return mDownloadStatus;
    }

    /**
     * WHAT'S GOING ON IN THIS execute() METHOD:
     * (1) Create a new instance of the DownloadRawData class (which extends AsyncTask class)
     * (2) Call this "execute" method of this new instance of the DownloadRawData class (which extends AsyncTask class)
     *
     * IMPORTANT POINTS TO REMEMBER:
     * - The ".execute" is part of the Async task class and its role is to start the process. It passes the "mRawURL"
     *    parameter to the "doInBackground" method.
     */
    public void execute(){
        this.mDownloadStatus = DownloadStatus.PROCESSING;
        /**
         * (1) Create a new instance of the DownloadRawData class (which extends AsyncTask class)
         */
        DownloadRawData downloadRawData = new DownloadRawData();
        /**
         * (2) Call this "execute" method of this new instance of the DownloadRawData class (which extends AsyncTask class)
         */
        downloadRawData.execute(mRawURL);
    }

    /**
     * WHY AsyncTask:
     * -- Downloads over the network is something that needs to occur asynchronously.
     * --- We don't want to stop our entire application from working while we are waiting for the download to complete.
     * --- We use the AsyncTask functionality to carry out downloads(over the network) outside the main UI thread.
     * --- This way, we do not block the UI thread is some sort of issue occurs (i.e. network issues) or if the download process takes a long time.
     * --- By using AsyncTask, our application can still respond to gestures such as button clicks, scrolls etc. while the download is occurring.
     *
     * -- AsyncTask is setup as a private class within a public class in the code below. The AsyncTask must be a PRIVATE method
     * --- One advantage is that we can put the declaration for the private class within another class.
     * --- Also, this would allow the AsyncTask to access private and public variables within the mother class.
     */
        private class DownloadRawData extends AsyncTask<String, Void, String>{

        /**
         * The above declaration has 3 generic types.
         * (a) The first: Is the URL from where data would be downloaded from. This goes with the "doInBackground" method. In this case, it’s a String.
         * (b) The second: Is for the progress indicator published during the background computation. Set it to "Void" if you do not plan to use the progress indicator.
         *     Can also be used if you have any updates you want to pass to the onProgressUpdate() method
         * (c) The third: Is for the results that would be returned back from the background to the calling process. That is, the return type of the doInBackground() method.
         *
         * IMPORTANT POINTS TO REMEMBER:
         * -- The first (parameter passed to the doInBackground() method) and third (parameter returned by the doInBackground() method) parameter for an AsyncTask are for the "doInBackground" method.
         * -- Remember to add "<uses-permission android:name="android.permission.INTERNET"/>" to the manifest file since you will need to access the internet
         *
         * WHAT'S GOING ON IN THIS AsyncTask CLASS:
         * (1) Download the network data using the doInBackground() method.
         */

        // Setup the variable that would be used to save the data from the network source (which could be an XML or JSON file)
        private String myExternalData;

        /**
         * IMPORTANT POINTS TO REMEMBER:
         * -- The doInBackground() method can accept a variable number of arguments.
         * -- In this case, we have "String... urls". This means that "doInBackground" method will accept one or many URLs.
         * -- So "String... urls" essentially works as an array of Strings.
         * -- However, if we plan to pass a single value, we only focus on "urls[0]", i.e. only the first url. If this is
         *    done, even if this method is sent more than one "url", only the first "url" is processed.
         *
         *  WHAT'S GOING ON IN THIS onPostExecute() METHOD:
         *  (1) Check to see if any data was passed to the doInBackground() method i.e. if a url address was passed to the
         *      doInBackground() method.
         *  (2) Next, initiate the data download process by calling the private downloadExternalData(String theURL)
         *     method within a try-catch block.
         *  (3) Finally, handle different types of exceptions in case a problem occurs during the download process. When
         *      processing files, we want to catch/handle any errors we get and thus prevent the application from
         *      crashing/letting the application return gracefully.
         **/
        @Override
        protected String doInBackground(String... urls) {

            /**
             * (1) Check to see if any data was passed to the doInBackground() method i.e. if a url address was passed to the
             *     doInBackground() method.
             */
            if(urls == null){
                return null;
            }
            try{
                /**
                 * (2) Next, initiate the data download process by calling the private downloadExternalData(String theURL)
                 *     method within a try-catch block.
                 */
                myExternalData = downloadExternalData(urls[0]);
            }
            /**
             * (3) Finally, handle different types of exceptions in case a problem occurs during hte download process. When
             *     processing files, we want to catch/handle any errors we get and thus prevent the application from
             *     crashing/letting the application return gracefully.
             **/
            catch(IOException e){
                return "Unable to download file.";
            }
            return myExternalData;
        }

        /**
         * IMPORTANT POINTS TO REMEMBER:
         * -- Since the doInBackground method process can take a long time, the onPostExecute() is only executed once the doInBackground method completes.
         * -- The doInBackground() method invokes the onPostExecute() method once it completes execution.
         * -- The doInBackground() method passes its return variable (myExternalData in this case)to the onPostExecute() method during the invokation process.
         *  i.e. "webData" == "myExternalData" == "mData"
         *
         *  WHAT'S GOING ON IN THIS onPostExecute() METHOD:
         *  (1) Assign the downloaded data to one of the private variables of the main class (i.e. mData) that contains this AsyncTask and use the
         *      getter, getmData() to pass it to other classes in this project.
         *  (2) Establish the status of the download process.
         **/
        protected  void onPostExecute(String webData){
            /**
             *  (1) Assign the downloaded data to one of the private variables of the main class (i.e. mData) that contains this AsyncTask and use the
             *      getter, getmData() to pass it to other classes in this project.
             */
            mData = webData;

            /**
             *  (2) Establish the status of the download process.
             **/
            // Check to see if the downloaded data, (i.e. "mData"), is empty
            if(mData == null){
                // If the there is no downloaded data, check to see if the URL string was empty
                if(mRawURL == null){
                    // The URL was empty or you did not send us a valid URL, do not initialize the download status
                    mDownloadStatus = DownloadStatus.NOT_INITIALIZED;
                }else{
                    // If mData was still null even though we had a valid URL, then there must have been an error accessing that URL
                    mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;
                }
            }else{
                // Success
                mDownloadStatus = DownloadStatus.OK;
            }
        }

        // Notice that this is a private method.
        private String downloadExternalData(String theURL) throws IOException{
            // Number of characters we plan to download at a time from the External file
            int BUFFER_SIZE = 2000;
            // The connection to the input stream
            InputStream is = null;
            // Temporary container for our data
            String ExternalContent = "";

            try{
                // Define the URL and get it ready for processing
                URL url = new URL(theURL);
                // Open a reference to that URL/website address. So, essentially open the URL. i.e. the website/resource address
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                // Setup timeouts in case we cannot download the file or if there is an error, we want to close the connection gracefully

                // Time to wait for an inputStream "read" before giving up
                conn.setReadTimeout(10000); // in ms i.e 10 secs in this case
                // Time to wait for an inputStream "connection" before giving up
                conn.setConnectTimeout(15000); // in ms i.e 15 secs in this case

                // Need to specify the method/type of Http call. We would use "Get" here
                conn.setRequestMethod("GET");
                // Allow input
                conn.setDoInput(true);

                // Get a response code to see what happened. We want a 200 response code for success. 404 is also another potential response code
                int response = conn.getResponseCode();

                /**At this point, we have initialized , setup and connected. but we are still yet to extract the data*/
                // Now get a connection to the inputStream.
                is = conn.getInputStream();
                // Since we setup a buffersize of 2000 characters, we can only download 2000 characters at a time.
                // We need to setup a looping process to download the entire data in "2000" character increments

                // To do this, we need an inputStream reader (and we need to pass our connection to the reader)
                // inputStream reader is an independent process. In this case, we are passing it the connection to a server file (hence "is").
                // It does not necessarily have to be a file, it does not really care about the format of what is passed to it. We are saying
                // that it should read through whatever we send it. Here we sent a connection "is" to it.
                InputStreamReader isr = new InputStreamReader(is);
                // This is the number of characters that has been read by the while loop below.
                // If the first time we call the "while" loop an its 4000 characters i.e. bytes, then we would only return 2000 characters.
                // However, if we are at the end of the file and its less than 2000 characters i.e. say 500 characters, we return only 500 characters.
                // So we use this variable to check one character at a time to see how many characters have been read from the buffer
                int charRead;
                // Create a buffer
                char[] inputBuffer = new char[BUFFER_SIZE];
                //Another try-catch block
                try{
                    // While we still have data, keep on reading it
                    // charRead returns the number of characters returned by the call "isr.read(inputBuffer)". It will start at
                    // the beginning of the data and each time it goes through the loop, it will read another 2000 characters.

                    // So the while loop essentially reads the data into the input buffer.
                    while((charRead = isr.read(inputBuffer))>0){
                        // Store the content of the current read interaction in the readString variable below starting from position 0.
                        // Convert the input buffer to String by starting at position 0 and and going up to and including the number of characters read.
                        // We use the "charRead" instead of 200 is because the last read may be only 5 characters. So we only want to actually store the
                        // actual number of characters we have.
                        String readString = String.copyValueOf(inputBuffer, 0, charRead);
                        // Aggregate the total contents read so far and not just the number of characters read during this iteration
                        // Store this in the "ExternalContent" variable defined above
                        ExternalContent += readString;
                        // Now that we have read that. clear out the inputBuffer
                        inputBuffer = new char[BUFFER_SIZE];
                    }

                    // Now, return the aggregated data stored in "xmlContent"
                    return ExternalContent;


                } catch(IOException e){
                    e.printStackTrace();
                    return null;
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    return null;
                } catch (SecurityException e) {
                    e.printStackTrace();
                    return null;
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    return null;
                }
            }finally{
                // No matter what, if there is an error or not, we want to execute the code in here
                if(is != null){
                    // Clears the memory allocated to the inputStream. Remember that the inputStream is the mechanism we use to do the download
                    is.close();
                }
            }
        }
    }

}
