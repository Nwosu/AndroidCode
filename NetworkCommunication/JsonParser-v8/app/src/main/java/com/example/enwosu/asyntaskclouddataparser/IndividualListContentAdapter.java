package com.example.enwosu.asyntaskclouddataparser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by enwosu on 11/21/2014.
 */

// By extending the ArrayAdapter class, we would have all the functionality of an ArrayAdapter but at the same time,
// have the ability to modify some of the methods to deal with our peculiar scenario
// We use generic there "DataObject" to ensure that this adapter can only work with data it can handle. In this case,
// the type is "DataObject"
public class IndividualListContentAdapter extends ArrayAdapter<DataObject> {

    /**
     * NOTE: A final variable can only be given a value when
     * (a) it's first declared
     * OR
     * (b) In the constructor.
     *
     * It cannot be changed after it's been given a value.
     *
     * As an interesting side note, you can only access data declared outside an anonymous record
     * that has been declared as final in an anonymous method
     */

    private final Context mContext; //Note: the context is the row
    private final int mLayoutResourceId;
    private final ArrayList<DataObject> mData;
    private final LayoutInflater inflater;


    // You need to create a constructor
    public IndividualListContentAdapter(Context context, int resource, ArrayList<DataObject> data) {
        super(context, resource, data);
        // In general, the context allows this Java class to access application specific resources and classes (i.e. layoutInflater class), as
        // well as up-calls for application-level operations such as launching activities, broadcasting and receiving intents etc.
        //
        // Regular Java classes cannot handle or inflate XML files. As a result, you need to pass it a context from and Activity ot other Android construts which can inflate XMLs
        // You need a context to allow this file to handle XML's. So by passing a context from an Activity (which can handle XML's), you will be able to handle XML's in this Java class (which is not an Activity)
        this.mContext = context;
        // We need to store the layout resource passed to the constructor in the list that will contain the data
        this.mLayoutResourceId = resource; // A reference to the xml used as the row template
        // We get a layout inflater from the context passed to the constructor. Rather than retrieving the inflater each time we need it, we store it here so we can use it whenever we need it.
        this.inflater = LayoutInflater.from(mContext);
        // A reference to the list used to to store the downloaded data. The goal is to put this data in the different rows using the XML row template
        this.mData = data;

    }

    // Next we have to override 2 methods of the base class (ArrayAdapter) in order to make our adapter work
    // (a) getItem()
    // (b) getView()
    // Both methods need to be public since they will be called by our ListView which lives in a different file.

    /**
     * Note: Although I did not define a getItem method in the Data Source (i.e. IndividualListContent class in this case),
     * We use the super method (i.e. method inheritance idea) to call a get each data item in the Data Source
     *
     * @param position
     * @return
     */
    @Override
    public DataObject getItem(int position) {
        return super.getItem(position);
    }

    // The getView method below will take the data and write it to the rows in the list based on the XML row template.
    // When the ListView scrolls items of the screen, tt will ask the adapter for a new view to display by calling the getView() method.
    // Essentially, the getView method is called every time it wants to display another item.

    // Note:
    // "position": The position parameter is used to refer to the position of hte item (in the list) that it needs to display
    // "convertView": If the ListView has a view it can reuse, it passes a reference to the reusable view through the "convertView".
    //                Remember, you only have a view you can re-use when a view scrolls out of the screen. So only create a new if
    //                ConvertView == null
    // "parent": A reference to the individual_list_fragment_elements.xml (i.e. the XML ViewGroup that contains all the different elements)
    //           It matches the resource code passed in the constructor
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //convertView is the row
        View row = convertView;

        // Instantiate a PlaceHolder class
        PlaceHolder holder = null;

        /**
         * Get the right position of the data source Arraylist (which was created earlier in the onCreate method of Activity that
         * calls this Adapter) based on the position passed to this getView method
         * Get Data Object For Each Row Based On The Row Position Passed To This Get View Method: So essential, we get the data we want to write on a particular row here.
         * The attributes of the data object will contain the data (i.e. the state/value of an attribute) for the different elements in each row i.e. text, image etc
         */
        final DataObject mListContent = mData.get(position);

        // The if(row == null), we should check to see if we currently don't have a row to re-use
        // The idea is to NOT re-inflate if already done
        if (row == null) {
            // Since we don't have any row to re-use, create one by inflating the layout for a single row. So we inflate the resource to create our view
            // The term "inflate" the resource means that we take the xml representation and use it as a template to generate the actual view object that will be displayed on the screen.

            // Turn the actual XML into a view by inflating it. Remember, mLayoutResourceId stores a reference to our row description XML. See the constructor above.
            // Remember, this row is a ViewGroup and contains other views
            row = inflater.inflate(mLayoutResourceId, parent, false); //false: the views have a hierarchy where they connect. So, here we don't want it to connect yet.

            holder = new PlaceHolder();

            /**
             * Do not proceed if the ArrayList does not exist (i.e. here, it does not exist if we cannot get a position.
             * Remember, a position of 0 is valid but you get null when there is no ArrayList)
             */
            if (mListContent != null) {
                // Store the reference to the different TextView elements in each row in the PlaceHolder class
                // Notice how we use "row.findViewById". we are essentially looking for the respective UI element within the row ViewGroup.
                holder.myLatitude = (TextView) row.findViewById(R.id.mLatitude);
                holder.myLongitude = (TextView) row.findViewById(R.id.mLongitude);
                holder.myRoadName = (TextView) row.findViewById(R.id.mRoadName);
                holder.myThumbnail = (ImageView) row.findViewById(R.id.mThumbnail);
                holder.myTitle = (TextView) row.findViewById(R.id.mTitle);
                row.setTag(holder);
            }
        } else {
            //Otherwise use an existing one
            holder = (PlaceHolder) row.getTag();
        }

        /**
         * Do not proceed if the ArrayList does not exist (i.e. here, it does not exist if we cannot get a position.
         * Remember, a position of 0 is valid but you get null when there is no ArrayList)
         */
        if (mListContent != null) {
            /*
            * Set the different view elements to reflect the data we need to display
            */
            // Now set the text for that particular row
            holder.myLatitude.setText(mListContent.getLatitude());
            holder.myLongitude.setText(mListContent.getLongitude());
            holder.myRoadName.setText(mListContent.getRoadName());
            holder.myTitle.setText(mListContent.getTitle());
            Picasso.with(mContext).load(mListContent.getImageURL())
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .into(holder.myThumbnail);
        }

        // Finally, you must return the row view (because this is called getView after all)
        return row;
    }

    private static class PlaceHolder {
        // This class is used to hold the views that we found the last time. It should have a field of all the widgets that are part of the view
        TextView myLatitude;
        TextView myLongitude;
        TextView myRoadName;
        TextView myTitle;
        ImageView myThumbnail;

    }

}
