package com.example.enwosu.asyntaskclouddataparser;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;

/** In order to download and use data from a network JSON file, the following has to happen
 * (1) Download the data
 * (2) Parse the data and store it in the object you created (DataObject)
 * (3) Retrieve the data object and create a list of these objects
 * (4) Display the relevant portions of the data on the UI thread using the onPostExecute() method (which has access to the UI thread).
 */

public class MainActivity extends Activity {
    private String LOG_TAG = MainActivity.class.getSimpleName();
    Button parseButton;
    ListView appList;
    GetRawData theRawData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * Now we must execute the AsyncTask
         * (1) Download the data from the URL using the doInBackGround method (which does not have access to the UI thread)
         */
        // Create an instance of the GetRawData class and pass the URL to its constructor. If you will recall, the GetRawData class contains the AsyncTask subclass
        theRawData = new GetRawData("http://www.wsdot.wa.gov/Traffic/api/HighwayCameras/HighwayCamerasREST.svc/GetCamerasAsJSON?AccessCode=39e5c3a5-14f7-416a-9b56-81b06107945b");
        // Next, call the "execute" method of the newly instantiated GetRawData class. The execute method invokes the AsyncTask class.
        theRawData.execute();

        
        appList = (ListView) findViewById(R.id.list_of_apps);
        parseButton = (Button) findViewById(R.id.btn_parse);
        parseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseJSON parseJSON;
                /**
                 * (2) Parse the data and store it in the object you created (DataObject)
                 *
                 * IMPORTANT POINTS TO REMEMBER:
                 * - The private variable, mData in the GetRawData class with contain the downloaded data after the AsyncTask class has been fully executed.
                 * - Now use the getter, getmData() to pass the private variable, mData, to the JSON parser in this file (from the GetRawData class)
                 * - The parser then parses and extracts the data into our DataObject class
                 * - Only do this if we have connectivity
                 *
                 */
                if (isOnline()) {
                    // You want to pass the data and download status associated with th same instance to the JSON parser code
                    parseJSON = new ParseJSON(theRawData.getmData(), theRawData.getmDownloadStatus());

                    // Now start the parsing process by calling the relevant method.
                    boolean myOperationStatus = parseJSON.processJSON();
                    if (myOperationStatus) {
                        /**
                         * (3) Retrieve the data object and create a list of these objects
                         **/
                        ArrayList<DataObject> records = parseJSON.getJSON_individual_data_record();

                        /**
                         * (4) Display the relevant portions of the data on the UI thread (i.e on the screen) using the onPostExecute() method (which has access to the UI thread).
                         **/
                        ArrayAdapter<DataObject> adapter = new IndividualListContentAdapter(MainActivity.this, R.layout.individual_list_fragment_elements, records);
                        appList.setVisibility(appList.VISIBLE);
                        appList.setAdapter(adapter);
                    } else {
                        Log.d("MainActivity", "Error parsing file");
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Network is not available", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    protected boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }else{
            return false;
        }
    }

}
