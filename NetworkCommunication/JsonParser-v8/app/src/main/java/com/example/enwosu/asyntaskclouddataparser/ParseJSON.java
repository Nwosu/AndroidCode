package com.example.enwosu.asyntaskclouddataparser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by enwosu on 12/29/2015.
 */
public class ParseJSON extends GetRawData {

    private String LOG_TAG = ParseJSON.class.getSimpleName();

    // Use to store the aggregated XMl data passed to this class. i.e. myExternalData  from the AsyncTask class in GetRawData
    private String data;

    // We need an array list to contain a list of data units from the aggregated JSON data passed from the AsyncTask class in MainActivity
    private ArrayList<DataObject> json_individual_data_record;

    // The status of the download process associated with the data we plan to parse
    DownloadStatus aDownloadStatus;

    /**
     * WHAT'S GOING ON IN THIS CONSTRUCTOR METHOD:
     * (1) Initialize the variable (i.e. "data") used to store the downloaded data
     * (2) Initialize the status of the download process associated with the specific downloaded data of interest
     * (3) Initialize the array list (i.e. "json_individual_data_record" that would be used to store the different JSON data
     *
     * IMPORTANT POINTS TO REMEMBER:
     * - If you recall, the MainActivity has an instance of GetRawData and invoked the AsyncTask associated with the GetRawData class.
     * - The MainActivity also invokes the the getmData() and getmDownloadStatus associated with the GetRawData class.
     * - As a result, it has access to both the downloaded data and the status of the associated download process.
     */

    public ParseJSON(String myExternalData, DownloadStatus mDownloadStatus) {
        super(null);
        /**
         * (1) Initialize the variable (i.e. "data") used to store the downloaded data
         */

        this.data = myExternalData;
        /**
         * (2) Initialize the status of the download process associated with the specific downloaded data of interest
         */
        this.aDownloadStatus = mDownloadStatus;

        /**
         * (3) Initialize the array list (i.e. "json_individual_data_record" that would be used to store the different JSON data
         */
        json_individual_data_record = new ArrayList<DataObject>();
    }

    // This getter will be used to access the list of entities downloaded. We want the calling process in (MainActivity in
    // this case) to be able to get the FINAL list of parsed data.
    public ArrayList<DataObject> getJSON_individual_data_record() {
        return json_individual_data_record;
    }

    /**
     * WHAT'S GOING ON IN THIS processJSON() METHOD:
     * (1) Initialize the variable (i.e. "operationStatus") used to store the status of the parsing process i.e. success or error
     *     By default, set this to true and only change it is there is an error
     * (2) Check to see if the data download process was successful. Only parse the data if the download process was successful.
     * (3) Identify the attributes of interest in the JSON file and set up variables for storing the data. These variables will
     *     map to the variables in your DataObject class (used to store each entity from the downloaded data)
     * (4) Parse the data.
     * (5) Finally, handle different types of exceptions in case a problem occurs during the download process. When
     *     parsing data, we want to catch/handle any errors we get and thus prevent the application from
     *     crashing/letting the application return gracefully.
     * (6) Return the "operationStatus".
     * IMPORTANT POINTS TO REMEMBER:
     * - Since the data starts with a curly bracket i.e. "{", we would need getJSONObject() to parse the file.
     * - If it started with a square bracket i.e. "[", we would have had to use getJSONArray().
     */
    public boolean processJSON(){
        /**
         * (1) Initialize the variable (i.e. "operationStatus") used to store the status of the parsing process i.e. success or error
         *     By default, set this to true and only change it is there is an error
         */
        boolean operationStatus = true;
        /**
         * (2) Check to see if the data download process was successful. Only parse the data if the download process was successful.
         */
        if (aDownloadStatus != DownloadStatus.OK){
            Log.v(LOG_TAG, "Error downloading raw file");
            operationStatus = false;
        }


        /**
         * (3) Identify the attributes of interest in the JSON file and set up variables for storing the data. These variables will
         *     map to the variables in your DataObject class (used to store each entity from the downloaded data)
         */
        final String CAMERA = "CameraLocation";
        final String LATITUDE = "Latitude";
        final String LONGITUDE = "Longitude";
        final String ROADNAME = "RoadName";
        final String IMAGE_URL = "ImageURL";
        final String TITLE = "Title";

        /**
         * (4) Parse the data.
         */
        try{
            // The below code transforms the JSON formatted object into an untyped array of objects
            JSONArray jsonData = new JSONArray(data);

            // However, we want to create a list of specifically typed objects (i.e. a list of type "DataObject").
            // The JSONArray class does not implement the iterable interface and so, we can't use a "for each" loop on JSONArray.
            // Instead, let's use a standard loop.
            for(int i = 0; i<jsonData.length(); i++){
                // Now each time through the loop, get a reference to the current JSON object using the getJSONObject method.
                JSONObject decomposeJSONArray = jsonData.getJSONObject(i);
                // Extract the Camera object associated with the current JSON object
                JSONObject cameraData = decomposeJSONArray.getJSONObject(CAMERA);
                // Extract the required attribute associated with the Camera object associated with the current JSON object
                String latitude = cameraData.getString(LATITUDE);
                String longitude = cameraData.getString(LONGITUDE);
                String roadName = cameraData.getString(ROADNAME);

                // Remember that title and image URL is not part of Camera Object but part of the object that contains the Camera object
                // That is, the title and image URL is part of the current JSON object and thus, on the same level with the Camera object
                // Extract both the title and image URL
                String title = decomposeJSONArray.getString(TITLE);
                String imageURL = decomposeJSONArray.getString(IMAGE_URL);

                // Store all the extracted data from the current JSON object in an instance of the DataObject.
                // Remember that DatObject is a Java class whose attributes were modelled after the data attributes we care about.
                // The goal is to use instances of this class to store the downloaded data
                DataObject storeJSONinObject = new DataObject(latitude, longitude, roadName, title, imageURL);

                // Add the DataObject which now contains the attributes we care about from the current JSON object to a list of DatObjects
                this.json_individual_data_record.add(storeJSONinObject);

            }
        /**
         * (5) Finally, handle different types of exceptions in case a problem occurs during the download process. When
         *     parsing data, we want to catch/handle any errors we get and thus prevent the application from
         *     crashing/letting the application return gracefully.
         */
        }catch(JSONException jsone){
            jsone.printStackTrace();
            Log.e(LOG_TAG, "Error processing Json data");
            operationStatus = false;
        }
        /**
         * (6) Return the "operationStatus".
         */
        return operationStatus;
    }

}
