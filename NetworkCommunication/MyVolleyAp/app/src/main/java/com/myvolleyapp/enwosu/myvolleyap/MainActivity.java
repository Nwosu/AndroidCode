package com.myvolleyapp.enwosu.myvolleyap;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    //Here the JSON data starts as an array (i.e. with the "[" bracket)
    private String urlArrayJSONData = "http://jsonplaceholder.typicode.com/posts";

    //Here the JSON data starts as an object (i.e. with the "{" bracket)
    private String  urlObjectJSONData= "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1#";
    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getJsonArray(urlArrayJSONData);
        getJsonObject(urlObjectJSONData);
    }

    // Handle JSOn Objects
    private void getJsonObject(String url){
        // The below "New JsonObjectRequest" takes a few parameters. The method can be a GET (get form server) or POST (send something to the server).

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (String)null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                Log.v("JSON Starts As Object:", response.toString()); // this should show all the data


                try {
                    JSONArray itemsArray = response.getJSONArray("items");

                    for (int i = 0; i < itemsArray.length(); i++) {
                        JSONObject anArrayElement = itemsArray.getJSONObject(i);
                        String title = anArrayElement.getString("title");
                        String author = anArrayElement.getString("author");
                        String author_id = anArrayElement.getString("author_id");
                        Log.v("Title - getJsonObject: ", title + "\n" + "Author: " + author + "\n" + "Author_ID: " + author_id);
                    }

                }catch(JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.d("Error.Response", response);
                Log.v(TAG, error.getMessage());
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void getJsonArray(String url) {
        // Create a request for a JSON Array since our url data starts with a square bracket 9i.e. an array and not an object with is the "{" bracket)
        JsonArrayRequest request = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                // At this point, we are ready to parse the data. The "response" parameter is the entire data from the url link.
                Log.v("JSON Starts As Array:", response.toString()); // this should show all the data
                // Here we can parse the data
                for (int i = 0; i < response.length(); i++) {
                    // For each array index for this JSON schema, we have an object that has 3 elements
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String title = jsonObject.getString("title");
                        String id = jsonObject.getString("id");
                        String body = jsonObject.getString("body");
                        Log.v("Title - getJsonArray", title + "\n" + "Id: " + id + "\n" + "Body: " + body);
                    } catch (JSONException e) {

                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // For handling errors
                VolleyLog.d("TAG", error.getMessage());
            }
        });
        // After doing all the work above, we want to add the request to the queue. This is where the global AppController singleton comes in.
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
