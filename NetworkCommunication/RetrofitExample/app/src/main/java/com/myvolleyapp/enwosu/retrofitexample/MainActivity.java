package com.myvolleyapp.enwosu.retrofitexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity{
    private String LOG_TAG = MainActivity.class.getSimpleName();
    ListView appList;
    public static final String ENDPOINT = "http://www.wsdot.wa.gov/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appList = (ListView) findViewById(R.id.list_of_apps);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Tell the app that APIOne is the class you want to use and how I am going to call it.
        RestInterface restInterface = retrofit.create(RestInterface.class);

        Call<List<DataObject>> call = restInterface.getFeed();
        call.enqueue(new Callback<List<DataObject>>() {
            @Override
            public void onResponse(Call<List<DataObject>> call, Response<List<DataObject>> response) {
                // Response.isSuccessful() is true if the response code is 2xx
                if (response.isSuccessful()) {
                    List<DataObject> records = response.body();
                    ArrayAdapter<DataObject> adapter = new ArrayAdapter<DataObject>(MainActivity.this, R.layout.list_item, records);
                    appList.setVisibility(appList.VISIBLE);
                    appList.setAdapter(adapter);
                } else {
                    int statusCode = response.code();
                    // Handle request errors yourself
                    ResponseBody errorBody = response.errorBody();
                }
            }

            @Override
            public void onFailure(Call<List<DataObject>> call, Throwable t) {
                // Handle execution failures like no internet connectivity
                Log.v(LOG_TAG, "Failure");
            }
        });
    }


}
