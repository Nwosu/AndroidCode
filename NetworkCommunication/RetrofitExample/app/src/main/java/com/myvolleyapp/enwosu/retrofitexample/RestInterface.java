package com.myvolleyapp.enwosu.retrofitexample;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by enwosu on 3/19/2016.
 *
 */
public interface RestInterface {
    // Declare one abstract method for each feed you want to use. The declaration will start with an annotation indicating the HTTP verb
    // Let's start with Get
    // Within the parenthesis, define the URL of the feed but don't include the base URL, only include the part after the root directory
    @GET("Traffic/api/HighwayCameras/HighwayCamerasREST.svc/GetCamerasAsJSON?AccessCode=39e5c3a5-14f7-416a-9b56-81b06107945b")
    // Next declare the method you want to use. when you declare the method, you'll pass in an instance of something called the call back class.
    // The "Callback" class is also a member of the retrofit library.
    // Be careful, there are a lot of Call back class libraries. scroll until you find the one from Retrofit and select that one
    // Next, you indicate what kind of data you want to get back. You could pass in a String but Retrofit knows how to take a json formatted bit
    // of content and transform it into strongly typed plain old java objects. However, there is a requirement:
    // (1) You Java object class must have names that exactly match the property names in your json content. If you want to get back an array of objects,
    //     then it must be declared as an array of objects in the json feed.
    public Call<List<DataObject>> getFeed();
}
